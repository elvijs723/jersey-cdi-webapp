package org.yry.service;

import java.util.List;

import org.yry.dao.entity.Address;
import org.yry.dao.entity.Person;

public interface PersonService {
	
	public Person get(Long id);
	public Person save(Person person);
	public List<Person> getAllPersons();
	public Person delete(Long id);
	public Address saveAddress(Long personid, Address address);
}	
