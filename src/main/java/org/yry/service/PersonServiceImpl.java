package org.yry.service;

import java.util.List;

import javax.inject.Inject;

import org.yry.dao.PersonRepository;
import org.yry.dao.entity.Address;
import org.yry.dao.entity.Person;

public class PersonServiceImpl implements PersonService{

	private PersonRepository personRepository;
	
    @Inject
    public PersonServiceImpl(PersonRepository personRepository)
    {
        this.personRepository = personRepository;
    }
    
	@Override
	public Person get(Long id) {
		return personRepository.get(id);
	}

	@Override
	public Person save(Person p) {
		return personRepository.save(p);
	}

	@Override
	public List<Person> getAllPersons() {
		return personRepository.getAllPersons();
	}

	@Override
	public Person delete(Long id) {
		return personRepository.delete(id);
	}

	@Override
	public Address saveAddress(Long personId, Address address) {
		return personRepository.saveAddress(personId, address);
	}
	
}
