package org.yry.resource;



import java.net.URI;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.process.internal.RequestScoped;
import org.yry.dao.entity.Address;
import org.yry.dao.entity.Person;
import org.yry.service.PersonService;

import com.fasterxml.jackson.databind.ObjectMapper;





/**
 * Root resource (exposed at "myresource" path)
 */
@RequestScoped
@Path("myresource")
public class MyResource {
	@Context UriInfo uriInfo; 
	@Context Request request;
	private final static Logger LOGGER = Logger.getLogger(MyResource.class.getName());
//	private Long counter = 0l;
	
//	@PersistenceContext(unitName = "mydb")
//	@PersistenceUnit(unitName="mydb")
//	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("asdf");
	@Inject
	private PersonService personService;
//	@PostConstruct
//	public void ini2t() {
//	emf = Persistence.createEntityManagerFactory("mydb");
//	}
	
	
	
    /**
     * 
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
	@GET
    @Path("{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Person GetUser(@PathParam("userId") Long userId){
//    	EntityManager em = emf.createEntityManager();
//    	return new PersonResource(uriInfo, emf.createEntityManager(), userId);
//    	Person p = new Person();
    	
//    	p.setName("John");
//    	em.persist(p);
//    	em.close();
//        return "Got it!";
    	LOGGER.log(Level.FINE, "asd");
    	return personService.get(userId);
    }
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
	public List<Person> getAllPersons(){
		return personService.getAllPersons();
	}
	
	@POST
	@Path("{userId}")
	@Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
	public Address addresses(@PathParam("userId") Long userId, Address address) {
		return personService.saveAddress(userId, address);
	}
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Person savePerson(Person person) {
//    	EntityManager em = emf.createEntityManager();
//    	em.getTransaction().begin();
    	personService.save(person);
//    	Person p = new Person();
//    	p.setName("John");
//    	em.persist(person);
//    	em.flush();
//    	em.getTransaction().commit();
//    	em.close();
//    	System.out.println(counter++);
    	return person;
    }
    
    @DELETE
    @Path("{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Person deletePerson(@PathParam("userId") Long userId) {
    	return personService.delete(userId);
    }
//    public static Set<Archive> findPersistenceArchives(ClassLoader loader){
//        // allow alternate persistence location to be specified via system property.  This will allow persistence units
//        // with alternate persistence xml locations to be weaved
//        String descriptorLocation = System.getProperty(PersistenceUnitProperties.ECLIPSELINK_PERSISTENCE_XML, PersistenceUnitProperties.ECLIPSELINK_PERSISTENCE_XML_DEFAULT);
//        return findPersistenceArchives(loader, descriptorLocation);
//    }
//    
   
//    @PUT
//    public Response putContainer(@Context) {
//        System.out.println("PUT CONTAINER " + container);
//     
//        URI uri = uriInfo.getAbsolutePath();
//        Container c = new Container(container, uri.toString());
//     
//        Response r;
//        if (!MemoryStore.MS.hasContainer(c)) {
//            r = Response.created(uri).build();
//        } else {
//            r = Response.noContent().build();
//        }
//     
//        MemoryStore.MS.createContainer(c);
//        return r;
//    }
}
