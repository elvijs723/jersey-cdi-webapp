package org.yry.resource;

import javax.persistence.EntityManager;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.yry.dao.entity.Person;

import com.fasterxml.jackson.databind.ObjectMapper;

public class AddressResource {

    private Long userId;
    private Person person;
    private UriInfo uriInfo;
    private EntityManager em;

    /**
     * Creates a new instance of UserResource
     */
    public AddressResource(UriInfo uriInfo, EntityManager em, Long userId) {
        this.uriInfo = uriInfo;
        this.userId = userId;
        this.em = em;
        person = em.find(Person.class, userId);
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Person getUserById() {
    	if(person==null) throw new NotFoundException("user" + userId +" not found");
    	return person;
    }
    
    private static String personObjectConverter(Object o) {
    	ObjectMapper om = new ObjectMapper();
    	String jsonObject;
    	try {
    		jsonObject = om.writeValueAsString(o);
    	}
    	catch(Exception e) {
    		throw new RuntimeException(e);
    	}
    	return jsonObject;
    }
}
