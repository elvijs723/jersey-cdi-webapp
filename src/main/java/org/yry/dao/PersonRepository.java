package org.yry.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.ws.rs.NotFoundException;

import org.glassfish.jersey.process.internal.RequestScoped;
import org.yry.dao.entity.Address;
import org.yry.dao.entity.Person;

//@RequestScoped
public class PersonRepository {

	private final EntityManagerFactory emf;
//	@PersistenceContext(name="asdf")
	private EntityManager em;
	
	public PersonRepository() {

		/* try {
	            InitialContext ctx = new InitialContext();
	            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/DefaultDB");

	            Map properties = new HashMap();
	            properties.put(PersistenceUnitProperties.NON_JTA_DATASOURCE, ds);
	            emf = Persistence.createEntityManagerFactory("persistence-with-jpa", properties);
	        } catch (NamingException e) {
	            throw new RuntimeException(e);
	        }*/
		emf = Persistence.createEntityManagerFactory("asdf");
		em = emf.createEntityManager();
	}
	
	public Person get(Long id) {
//		EntityManager em = emf.createEntityManager();
		Person p = em.find(Person.class, id);
		if(p==null) throw new NotFoundException();
		em.close();
		return p;
	}
	
	public Person save(Person p) {
//		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.persist(p);
		em.getTransaction().commit();
		em.close();
		return p;
	}

	public Person delete(Long id) {
		em.getTransaction().begin();
//		Person p = em.find(Person.class,id);
		Person p = em.find(Person.class,id);
		em.remove(p);
		em.getTransaction().commit();
		em.close();
		return p;
	}
	
	public Address saveAddress(Long personId, Address address) {
		em.getTransaction().begin();
		Person p = em.find(Person.class, personId);
		List<Address> list = p.getAdresses();
		
		list.add(address);
		
		em.getTransaction().commit();
		return address;
	}
	
	public List<Person> getAllPersons(){
		em.getTransaction().begin();
		List<Person> results = em.createNamedQuery("Person.findAll").getResultList();
		return results;
	}
}

//public class UserRepository  {
//    @PersistenceContext(name = "persistence-unit")  // name is Persistence Unit Name configured in persistence.xml
//    private EntityManager entityManager;
//
//    @Override
//    public EntityManager getEntityManager() {
//        return entityManager;
//    }
//
//    @Override
//    public Class<User> getEntityClass() {
//        return User.class;
//    }
//}