package org.yry.dao.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Persons")
@NamedQuery(name="Person.findAll", query="select p from Person p")
public class Person {
	
	@Id @GeneratedValue
	Long id;
	
	String name;
	

//	@JoinColumn(name = "post_id")
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy="person")
	private List<Address> adresses;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Address> getAdresses() {
		return adresses;
	}

	public void setAdresses(List<Address> adresses) {
		this.adresses = adresses;
	}
	
	
	
}
