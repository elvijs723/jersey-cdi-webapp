package org.yry.application;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.yry.dao.PersonRepository;
import org.yry.service.PersonService;
import org.yry.service.PersonServiceImpl;

public class MyApplicationBinder extends AbstractBinder {
	@Override
	protected void configure() {
		bind(PersonServiceImpl.class).to(PersonService.class);
		bind(PersonRepository.class).to(PersonRepository.class);
	}

}
