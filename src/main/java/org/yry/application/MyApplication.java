package org.yry.application;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.jackson.JacksonFeature;
//import org.glassfish.jersey.jsonp.JsonProcessingFeature;
//import org.glassfish.jersey.jettison.JettisonFeature;
import org.glassfish.jersey.server.ResourceConfig;

//@ApplicationPath("/")
public class MyApplication extends ResourceConfig{
	public MyApplication() {
		packages("org.yry");
//		register(new JettisonFeature());
		register(JacksonFeature.class);
//		register(JsonProcessingFeature.class);
		register(ObjectMapperProvider.class);
		register(new MyApplicationBinder());
	}
}
